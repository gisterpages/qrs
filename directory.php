<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
  <div id="wrapper" class="home-page">

    <!-- start header -->
    <?php include 'includes/topbar.php'; ?>
    <?php include 'includes/header.php'; ?>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="pageTitle">Directory</h2>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="about-logo">
            <h3>Certification Directory</span></h3>
            <p>Please find your certificates by entering the certification number below. </p>
            <form class="form-inline">
            <div class="form-group">
              <input type="text" class="form-control" id="certification" placeholder="Certification Number">
            </div>
            <button type="submit" class="btn btn-default" style="padding: 7px 20px">Submit</button>
          </form>
          </div>
        </div>
      </div>

    </div>
    </section>

    <!-- start footer -->
    <?php include 'includes/footer.php'; ?>
    <!-- end footer -->
  </div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fancybox.pack.js"></script>
  <script src="js/jquery.fancybox-media.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <!-- Vendor Scripts -->
  <script src="js/modernizr.custom.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/custom.js"></script>
</body>

</html>
