<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
	<div id="wrapper" class="home-page">

		<!-- start header -->
		<?php include 'includes/topbar.php'; ?>
		<?php include 'includes/header.php'; ?>
		<!-- end header -->

		<section id="banner">

			<!-- Slider -->
			<div id="main-slider" class="flexslider">
				<ul class="slides">
					<li>
						<img src="img/slides/1.jpg" alt="" />
						<div class="flex-caption">
							<h3>ISO</h3>
							<p>Standards Developed</p>

						</div>
					</li>
					<li>
						<img src="img/slides/2.jpg" alt="" />
						<div class="flex-caption">
							<h3>Management <br/>Systems</h3>
							<p>On Environmental, health, food safety and more...</p>

						</div>
					</li>
				</ul>
			</div>
			<!-- end slider -->

		</section>
		<section id="call-to-action-2">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-sm-9">
						<h3>Ensure Quality</h3>
						<p>Quality Registration Services is an expertised Assessment Body carrying known reputation in the field of Management systems certification. Through renowned reputation gained in the market, we have been serving various societies ISO Certifications.</p>
					</div>
					<div class="col-md-2 col-sm-3">
						<a href="iso.php" class="btn btn-primary">Read More</a>
					</div>
				</div>
			</div>
		</section>

		<section id="content">

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="aligncenter">
							<h2 class="aligncenter">Our Services</h2>
						</div>
						<br />
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4 info-blocks">
						<i class="icon-info-blocks fa fa-bell-o"></i>
						<div class="info-blocks-in">
							<h3>ISO</h3>
							<p>	ISO (International Organization for Standardization) is the world’s largest developer of voluntary International Standards.</p>
						</div>
					</div>
					<div class="col-sm-4 info-blocks">
						<i class="icon-info-blocks fa fa-hdd-o"></i>
						<div class="info-blocks-in">
							<h3>Management Systems</h3>
							<p> A management system is developed by organizations against core parameters like Quality, Environmental, health and safety, Food safety, Information security etc.</p>
						</div>
					</div>
					<div class="col-sm-4 info-blocks">
						<i class="icon-info-blocks fa fa-lightbulb-o"></i>
						<div class="info-blocks-in">
							<h3>Registrations</h3>
							<p>QRS possess versatile auditors to analyze, assess and recommend organizations for management system standards.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 info-blocks">
						<i class="icon-info-blocks fa fa-code"></i>
						<div class="info-blocks-in">
							<h3>Recognitions</h3>
							<p>	Certification or registration for an organization or an individual performing a task by recognizing the competency level against described standards is the conventional means of delivering assurance</p>
						</div>
					</div>
					<div class="col-sm-4 info-blocks">
						<i class="icon-info-blocks fa fa-compress"></i>
						<div class="info-blocks-in">
							<h3>Directory</h3>
							<p>Certifications are downloadable by entering your certificate number.</p>
						</div>
					</div>
					<div class="col-sm-4 info-blocks">
						<i class="icon-info-blocks fa fa-html5"></i>
						<div class="info-blocks-in">
							<h3>Contact us</h3>
							<p>We are open to discuss on all working day and provide a best consultation</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include 'includes/footer.php'; ?>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<!-- Vendor Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>
