<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
	<div id="wrapper">
		<!-- start header -->
		<?php include 'includes/topbar.php'; ?>
		<?php include 'includes/header.php'; ?>
		<!-- end header -->

		<section id="inner-headline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="pageTitle">About Us</h2>
					</div>
				</div>
			</div>
		</section>

		<section id="content">
			<section class="section-padding">
				<div class="container">
					<div class="row showcase-section">
						<div class="col-md-6">
							<img src="img/dev1.png" alt="showcase image">
						</div>
						<div class="col-md-6">
							<div class="about-text">
								<h3>Company</h3>
								<p>Quality Registration Services, a service oriented assessor, has flowered its operations all over the globe with the aim of serving international business markets with registration services for Management Systems. It has been the motto of QRS from the commencement to deliver purity in certification for end users to avail the supplier services. Over the decades, QRS has gained enormous reputation amongst various business sectors for delivering free, fair, impartial and ethical assessments. The moment our customers drive past us they seem to be growing bigger when we drive past them.</p>
								<br>
								<p> Quality Registration Services is an expertised Assessment Body carrying known reputation in the field of Management systems certification. Through renowned reputation gained in the market, we have been serving various societies with ISO Certifications. Founded by a group of committed professionals, we have been able to fast expand our registrations across continents. Governed by the board of directors and monitored by an impartial committee, the decision to register organization’s for management systems have been appreciated by stakeholders, clients and client of clients. Our recognition in national markets have been appreciated and accredited by International world council for certification bodies. Ever since the foundation, QRS has managed to branch its expertise across different societies. Competency levels of our auditors have never been compromised with the intention of delivering assured control. We believe in matured systems which can instill confidence in the supply-chain demand, thereby delivering the needed with the want. QRS has evaluated the possibilities of risk to its impartiality and has stringent procedures to cut down the sources that bring conflict of interest. Our service to different markets has been fulfilled by certification partners who have been pre-qualified and constantly evaluated to carry the services of QRS.

								</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			<div class="container">
				<div class="about">
					<div class="row">
						<div class="col-md-6">
							<!-- Heading and para -->
							<div class="block-heading-two">
								<h3><span>Mission</span></h3>
							</div>
							<p>QR Services dedicates its experience for building up confidential societies to the end users avail genuine deliverable from dependent suppliers. Achieve 100% purity in certifications is the theme driving QRS for several decades.</p>
						</div>
						<div class="col-md-6">
							<div class="block-heading-two">
								<h3><span>Vision</span></h3>
							</div>
							<p> Registrations for non-profitability and principle oriented assessment of systems to reach the untouched markets. Maintain the confidentiality with clients and high levels of competency is the objective driven from our policies.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php include 'includes/footer.php'; ?>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<!-- Vendor Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>
