<header>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">
                    <!--<img src="img/logo.jpg" alt="Quality Registration Services" style="width:50px;height:50px"/>--> Quality <br />Registration<br />Service</a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $selPage == 0 ? 'active' : '' ?>"><a href="index.php">Home</a></li>
                    <li class="dropdown <?php echo $selPage >= 1 && $selPage <= 3 ? 'active' : '' ?>">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">About Us <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="about.php">Company</a></li>
                            <li><a href="registration-policy.php">Registration Policy</a></li>
                            <li><a href="principles.php">Our Principles</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo $selPage == 4 ? 'active' : '' ?>"><a href="iso.php">ISO</a></li>
                    <li class="<?php echo $selPage == 5 ? 'active' : '' ?>"><a href="management-systems.php">Management Systems</a></li>
                    <li class="<?php echo $selPage == 6 ? 'active' : '' ?>"><a href="registrations.php">Registrations</a></li>
                    <li class="<?php echo $selPage == 7 ? 'active' : '' ?>"><a href="recognitions.php">Recognitions</a></li>
                    <li class="<?php echo $selPage == 8 ? 'active' : '' ?>"><a href="directory.php">Directory</a></li>
                    <li class="<?php echo $selPage == 9 ? 'active' : '' ?>"><a href="contact.php">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
