<?php
$uri = $_SERVER['REQUEST_URI'];
$urlArr = [
    'index',
    'about',
    'registration-policy',
    'principles',
    'iso',
    'management-systems',
    'registrations',
    'recognitions',
    'directory',
    'contact'
];
$selPage = 0;
foreach ($urlArr as $key => $val) {
    $a = strpos($uri, $val);
    if ($a) {
        $selPage = $key;
    }
}
?>

<head>
  <meta charset="utf-8">
  <title>Quality Registration Service - <?php echo $selPage == 0 ? 'home': $urlArr[$selPage]; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="http://webthemez.com" />
  <!-- css -->
  <link href="css/bootstrap.min.css" rel="stylesheet" />
  <link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
  <link href="css/flexslider.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
