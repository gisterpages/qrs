<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
	<div id="wrapper">
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="pull-left hidden-xs">We are Hiring!!</p>
						<p class="pull-right"><i class="fa fa-phone"></i>Tel No. (+001) 123-456-789</p>
					</div>
				</div>
			</div>
		</div>
		<!-- start header -->
		<?php include "includes/header.php"; ?>
		<!-- end header -->
		<section id="inner-headline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="pageTitle">Contact Us</h2>
					</div>
				</div>
			</div>
		</section>
		<section id="content">

			<div class="container">
				<div class="row">

					<div class="row">


						<div class="col-lg-5" style="border-radius:5px;">
							<p><span style="font-size:29px;">Johnny Viva</span></p>
							<p><span style="font-size:15px;padding-top: 50px;">Certification coordinator (Global operations)</span></p>
							<p><span class="glyphicon glyphicon-envelope"></span>&nbsp;certification@qrscertification.in</p>
							<p>Certification ISO 9001, ISO 14001, OHSAS 18001, ISO 22716, ISO 22000, HACCP, ISO 27001, ISO 29001, ISO 13485, ISO 50001, ISO 10002, ISO 20121, ISO22301, ISO 20000, QRS ISM 91418</p>
							<p style="font-size:20px;">Auditor Training, Personnel Certification</p>
						</div>

						<div class="col-lg-5 col-lg-offset-2" style="border-radius:5px;">
							<p><span style="font-size:29px;">Ram</span></p>
							<p><span style="font-size:15px;">Auditor</span></p>
							<p><span class="glyphicon glyphicon-envelope"></span>&nbsp;ram@qrscertification.in</p>
							<p>Certification ISO 9001, ISO 14001, OHSAS 18001, ISO 22716, ISO 22000, HACCP, ISO 27001, ISO 29001, ISO 13485, ISO 50001, ISO 10002, ISO 20121, ISO22301, ISO 20000, QRS ISM 91418</p>
							<p style="font-size:20px;">Auditor Training, Personnel Certification</p>
						</div>

					</div>
					<div class="row">
						<div class="col-md-6">


							<!-- Form itself -->
							<form name="sentMessage" id="contactForm" novalidate>
								<h3>Contact me</h3>
								<div class="form-group">
									<div class="controls">
										<input type="text" class="form-control" placeholder="Full Name" id="name" required data-validation-required-message="Please enter your name" />
										<p class="help-block"></p>
									</div>
								</div>
								<div class="form-group">
									<div class="controls">
										<input type="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Please enter your email" />
									</div>
								</div>

								<div class="form-group">
									<div class="controls">
										<textarea rows="10" cols="100" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter your message" minlength="5" data-validation-minlength-message="Min 5 characters" maxlength="999" style="resize:none; margin-top: 10px;"></textarea>
									</div>
								</div>
								<div id="success"> </div> <!-- For success/fail messages -->
								<div class="form-group">
								<button type="submit" class="btn btn-primary pull-right">Send</button><br />
								</div>
							</form>
						</div>
						<div class="col-md-6">
							<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
							<div style="overflow:hidden;height:500px;width:600px;">
								<div id="gmap_canvas" style="height:500px;width:600px;"></div>
								<style>
									#gmap_canvas img {
										max-width: none !important;
										background: none !important
									}
								</style><a class="google-map-code" href="http://www.trivoo.net" id="get-map-data">trivoo</a>
							</div>
							<script type="text/javascript">
								function init_map() {
									var myOptions = {
										zoom: 14,
										center: new google.maps.LatLng(40.805478, -73.96522499999998),
										mapTypeId: google.maps.MapTypeId.ROADMAP
									};
									map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
									marker = new google.maps.Marker({
										map: map,
										position: new google.maps.LatLng(40.805478, -73.96522499999998)
									});
									infowindow = new google.maps.InfoWindow({
										content: "<b>The Breslin</b><br/>2880 Broadway<br/> New York"
									});
									google.maps.event.addListener(marker, "click", function() {
										infowindow.open(map, marker);
									});
									infowindow.open(map, marker);
								}
								google.maps.event.addDomListener(window, 'load', init_map);
							</script>
						</div>
					</div>
				</div>

		</section>
		<?php include "includes/footer.php"; ?>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<!-- Vendor Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>

	<script src="contact/jqBootstrapValidation.js"></script>
	<script src="contact/contact_me.js"></script>
</body>

</html>
