<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
  <div id="wrapper" class="home-page">

    <!-- start header -->
    <?php include 'includes/topbar.php'; ?>
    <?php include 'includes/header.php'; ?>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="pageTitle">Recognitions</h2>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="about-logo">
            <h3>Recognitions</span></h3>
            <p>	Certification or registration for an organization or an individual performing a task by recognizing the competency level against described standards is the conventional means of delivering assurance to first parties by third parties. However, the recognition of a certification body performing a professional task such as auditing and inspection services is essential to instill confidence on certified parties. Thereby, recognition of professional firms by governmental and non-governmental bodies who recognize themselves and recognize certification bodies is accreditation. An accredited certificate means a lot in trans –continental trade. Industries desiring to share their products and services across markets shall have their systems certified by accredited certification organizations. Over the decades, governments and trade unions have recognized the necessity of accreditation bodies and have generated policies for healthy trade. Quality Registration Services with global operations and having been a lead assessor in the certification industry do takes into account the accreditation as a serious business and has never failed from it. </p>
            <p>The confidence level provided to our clients has always been on the rise and our services have been recognized globally by many governments and accreditation bodies. Though we are not an accredited assessment body, we do take into account the level of trust our clients have on us and thereby not compromising the audits or any form of service provided to our clients. QRS is working on accreditation in accordance to ISO 17021 and though this is a time consuming process, we hope to achieve it at the earliest to instill more confidence in our clients. For certificates to be reputable and accepted worldwide, accreditation of third party certification bodies (although not always mandatory) is the strong preference of many suppliers </p>
            <p>ISO/IEC 17021 Conformity assessment — Requirements for bodies providing audit and certification of management systems, as it is officially called, was prepared by the ISO Committee on conformity assessment (CASCO) in 2006. It was developed to fulfill the need to have an International Standard that could facilitate the recognition of bodies that were performing conformity assessments and the acceptance of their certifications on a national and international basis; making it easier to recognize management system certification in the interests of international trade. This International Standard provides a set of requirements for management systems auditing at a generic level, aimed at providing a reliable determination of conformity to the applicable requirements for certification, conducted by a competent audit team, with adequate resources and following a consistent process, with the results reported in a consistent manner.</p>
            <p>ISO 17021 is intended for use by CBs that carry out audit and certification of management systems. It gives generic requirements for such CBs performing audit and certification in the field of quality, environmental and other forms of management systems. These CBs (also called Registrars) can be non-governmental or governmental (with or without regulatory authority) and they do not need to offer all types of management system certification </p>
          </div>
        </div>
      </div>

    </div>
    </section>

    <!-- start footer -->
    <?php include 'includes/footer.php'; ?>
    <!-- end footer -->
  </div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fancybox.pack.js"></script>
  <script src="js/jquery.fancybox-media.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <!-- Vendor Scripts -->
  <script src="js/modernizr.custom.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/custom.js"></script>
</body>

</html>
