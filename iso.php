<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
  <div id="wrapper" class="home-page">
    <!-- start header -->
    <?php include 'includes/topbar.php'; ?>
    <?php include 'includes/header.php'; ?>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="pageTitle">ISO</h2>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO</span></h3>
              <p> ISO (International Organization for Standardization) is the world’s largest developer of voluntary International Standards. International Standards give state of the art specifications for products, services and good practice, helping to make industry more efficient and effective. Developed through global consensus, they help to break down barriers to international trade. </p>
              <p>ISO develops International Standards. Founded in 1947, and since then have published more than 19 500 International Standards covering almost all aspects of technology and business. From food safety to computers, and agriculture to healthcare, ISO International Standards impact all our lives. </p>
              <p>ISO is an independent, non-governmental organization made up of members from the national standards bodies of 164 countries with a Central Secretariat in Geneva, Switzerland, that coordinates the system. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>History</span></h3>
              <p>The ISO story began in 1946 when delegates from 25 countries met at the Institute of Civil Engineers in London and decided to create a new international organization ‘to facilitate the international coordination and unification of industrial standards’. In February 1947 the new organisation, ISO, officially began operations.</p>
              <p>Since then, it has published over 19 500 International Standards covering almost all aspects of technology and manufacturing. Today members from 164 countries and 3 368 technical bodies are there to take care of standard development. More than 150 people work full time for ISO’s Central Secretariat in Geneva, Switzerland.</p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/isohistory.jpg" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>Development</span></h3>
              <p>An ISO standard is developed by a panel of experts, within a technical committee. Once the need for a standard has been established, these experts meet to discuss and negotiate a draft standard. As soon as a draft has been developed it is shared with ISO’s members who are asked to comment and vote on it. If a consensus is reached the draft becomes an ISO standard, if not it goes back to the technical committee for further edits </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/development.jpg" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>Role in standardization</span></h3>
              <p>Some of ISO’s most well-known standards are management system standards. They provide a model to follow when setting up and operating a management system. Like all ISO standards, they are the result of international, expert consensus. Therefore, by implementing a management system standard, organizations can benefit from global management experience and good practice. These standards can be applied to any organisation, large or small, whatever its product or service and regardless of its sector of activity. An effective management system has many benefits including: </p>
              <ol>
                <li>More efficient resource use</li>
                <li>Improved risk management</li>
                <li>Increased customer satisfaction as services and products consistently deliver what they promise</li>
              </ol>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/role_in_standardisation.jpg" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>Achievements</span></h3>
              <p>ISO International Standards ensure that products and services are safe, reliable and of good quality. For business, they are strategic tools that reduce costs by minimizing waste and errors and increasing productivity. They help companies to access new markets, level the playing field for developing countries and facilitate free and fair global trade </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/achievements.png" class="iso-image">
          </div>
        </div>

      </div>
    </section>

    <!-- start footer -->
    <?php include 'includes/footer.php'; ?>
    <!-- end footer -->
  </div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fancybox.pack.js"></script>
  <script src="js/jquery.fancybox-media.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <!-- Vendor Scripts -->
  <script src="js/modernizr.custom.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/custom.js"></script>
</body>

</html>
