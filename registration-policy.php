<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
	<div id="wrapper">

		<!-- start header -->
		<?php include 'includes/topbar.php'; ?>
		<?php include 'includes/header.php'; ?>
		<!-- end header -->
		<section id="inner-headline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="pageTitle">About Us</h2>
					</div>
				</div>
			</div>
		</section>
		<section id="content">
			<section class="section-padding">
				<div class="container">
					<div class="row showcase-section">
						<div class="col-md-6">
							<img src="img/dev1.png" alt="showcase image">
						</div>
						<div class="col-md-6">
							<div class="about-text">
								<h3>Registration Policy</h3>
								<p>Quality Registration Services desired and desires to drive a renowned registration program in the field of management systems certification. The management of QRS understands the responsibility and accountability that it holds in providing fair and impartial assessment to clients aspiring for better systems. Our recognition for management systems certification instills the responsibility on QRS to eliminate the threats arising within and around for maintaining impartiality. Our commitment to inspire the society through transparent administration is being achieved by committees that encompass clients, clients of clients, recognized governmental authorities of the respective operational areas, non-governmental organizations, stake holders, consumers and public. We aspire to maintain a globalized high standard certification program in all aspects of certification through </p>
								<ol>
									<li> Providing world class assessments on management systems through committed auditing professionals.</li>
									<li> Creating awareness among clients on management system certifications.</li>
									<li> Compliance to accreditation requirements</li>
									<li> Deal complaints logically and ethically.</li>
									<li> Improve the levels of competency for all certification functions through constant training programmes and evaluations.</li>
									<li> Systematic approach to certification activities.</li>
									<li> Openness in administration to satisfy stake holders and interested personnel.</li>
									<li> Non-disclosure of confidential matters to third parties.</li>
									<li> Eliminate threats to certification process</li>
									<li> Maintain 100% conformity with requirements</li>
									<li> Stringent evaluation procedures on outsourcing</li>
									<li> Communicate our policy to public and interested parties</li>
									<li> Review and revise policies for sustainability.</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="container">

				<div class="about">


					<div class="row">
						<div class="col-md-6">
							<!-- Heading and para -->
							<div class="block-heading-two">
								<h3><span>Mission</span></h3>
							</div>
							<p>QR Services dedicates its experience for building up confidential societies to the end users avail genuine deliverable from dependent suppliers. Achieve 100% purity in certifications is the theme driving QRS for several decades.</p>
						</div>
						<div class="col-md-6">
							<div class="block-heading-two">
								<h3><span>Vision</span></h3>
							</div>
							<p> Registrations for non-profitability and principle oriented assessment of systems to reach the untouched markets. Maintain the confidentiality with clients and high levels of competency is the objective driven from our policies.</p>
						</div>
					</div>
					<br>
		</section>
		<?php include 'includes/footer.php'; ?>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<!-- Vendor Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>
