<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
  <div id="wrapper" class="home-page">

    <!-- start header -->
    <?php include 'includes/topbar.php'; ?>
    <?php include 'includes/header.php'; ?>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="pageTitle">Registrations</h2>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>Registration with QRS</span></h3>
              <p>QRS possess versatile auditors to analyze, assess and recommend organizations for management system standards. For decades, QRS has recognized several organizations in different business sectors for various ISO management system standards ranging from ISO 9001, ISO 14001 etc. Our recognition with accreditations has helped not to limit our scope of certification. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 9001 Quality Management System</span></h3>
              <p>ISO 9001 and it requirements have brought quality into the world of senior management and the boardroom where business decisions are made, objectives set and direction given. If a management system is developed based on the principles of ISO 9001, it redirects an organization to create a business sustainability management system as well as meeting the requirements, i.e. creating something that meets the needs of the present and the future. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 14001 Environmental Management System</span></h3>
              <p>ISO 14001 is an environmental management standard. It specifies a set of environmental management requirements for environmental management systems. The purpose of this standard is to help all types of organizations to protect the environment, to prevent pollution, and to improve their environmental performance. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 45001 Occupational Health and Safety Assessment Series</span></h3>
              <p>ISO 45001 Occupational Health and Safety Standard uses a management approach tool called the PDCA cycle. PDCA is an ongoing process that enables an organization to establish, implement and maintain its health and safety policy based on top management leadership and commitment to the safety management system. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 27001 Information Security Management System</span></h3>
              <p>ISO/IEC 27001 formally specifies an Information Security Management System (ISMS), a suite of activities concerning the management of information security risks. The ISMS is an overarching management framework through which the organization identifies, analyzes and addresses its information security risks. The ISMS ensures that the security arrangements are fine-tuned to keep pace with changes to the security threats, vulnerabilities and business impacts - an important aspect in such a dynamic field, and a key advantage of ISO27k’s flexible risk-driven approach as compared to, say, PCI-DSS.</p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 22716 Good Manufacturing Practice</h3>
              <p>ISO 22716 focuses on quality assurance in cosmetic industries as a means of producing and delivering safe product to the consumer sector. The standard highlights the importance of developing a controlled environment and systematic approach to production and logistic process to ensure the development of a safer product.</p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 22000 Food Safety Management System and HACCP</h3>
              <p>Food safety management system and Hazard Analysis and critical control point are best suitable for food oriented industries. The primary goal of the standards is to ensure the safety of food products and related services by implementing controlled conditions developed in par with Quality management system. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO 29001 Quality Management System For Oil, Gas and Petrochemical Industries</h3>
              <p>In order to best serve the interests of the industry, and as a method of better ensuring the safety of personnel and the environment, engineers, purchasers, users, manufacturers, service organizations and suppliers ISO/TS 29001 has been developed for oil, gas and petrochemical industries. </p>
              <p>Which companies can apply for ISO/ TS 29001? Organizations involved in exploration, production, pipelines and transportation, and refining of petroleum and natural gas products.</p>
              <p>Organizations involved in the design, manufacture, installation, service and repair of equipment used in the exploration, production, transportation and refining of petroleum and natural gas products </p>
              <p>Organizations that provide technical, operational and support services to the various industry sectors identified above.</p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>ISO/TS 16949 Automotive Quality Management</h3>
              <p>The ISO/TS16949 is an ISO technical specification aiming to the development of a quality management system that provides for continual improvement, emphasizing defect prevention and the reduction of variation and waste in the supply chain. It is based on the ISO 9001. ISO/TS 16949 is intended to build up or enforce the confidence of a (potential) customer into the system and process quality of a (potential) supplier. </p>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/img1.png" class="iso-image">
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="about-logo">
              <h3>Registration Process of QRS</h3>
              <p>QRS has developed a unique and comprehensive approach towards clients seeking management system registration. Our transparency in the recognition process facilitates the smooth flow of information on both sides. </p>
              <p>Get Registered with Quality Registration Services and serve tier 1 suppliers. </p>
              <ol>
                <li>Provide us the enquiry and we shall get back with the Registration form that declares the organization details, employees and the scope of registration. This information helps us serve you better with our financial part.</li>
                <li>Get our quote.</li>
                <li>Register for auditing services with a non-disclosure agreement accepting the registration terms and conditions of QRS, if our financial proposal satisfied you.</li>
                <li>Document Review and Initial Visit: (Pre-Conformity Assessment): Upon mutual agreement between both the parties, QRS shall be presented with management system documents. The review shall be carried out in QRS operations or client’s premises. The maturity of the management system will be assessed and reported to the organization for further improvements.</li>
                <li>Initial Assessment: (Conformity Assessment): If the client is ready for a complete system audit with comments from pre-conformity assessment sorted out, QRS shall audit and decide the registration of the organization.</li>
                <li>Registration: Upon successful completion of conformity assessment, the organization shall be presented with registration to specific management system and shall be listed in the registration directory of QRS in three days’ time.</li>
              </ol>
              <h5>A successful certification cycle will encompass routine surveillances followed by renewal of registration.</h5>
            </div>
          </div>
          <div class="col-md-6">
            <br />
            <img src="img/registrations.png" class="iso-image">
          </div>
        </div>
      </div>
    </section>
    <!-- start footer -->
    <?php include 'includes/footer.php'; ?>
    <!-- end footer -->
  </div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fancybox.pack.js"></script>
  <script src="js/jquery.fancybox-media.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <!-- Vendor Scripts -->
  <script src="js/modernizr.custom.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/custom.js"></script>
</body>

</html>
