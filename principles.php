<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
	<div id="wrapper">

		<!-- start header -->
		<?php include 'includes/topbar.php'; ?>
		<?php include 'includes/header.php'; ?>
		<!-- end header -->
		<section id="inner-headline">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="pageTitle">About Us</h2>
					</div>
				</div>
			</div>
		</section>
		<section id="content">
			<section class="section-padding">
				<div class="container">
					<div class="row showcase-section">
						<div class="col-md-6">
							<img src="img/dev1.png" alt="showcase image">
						</div>
						<div class="col-md-6">
							<div class="about-text">
								<h3>Our Principles</h3>
								<p>QRS has been driven by the following principles to maintain a world-class registration body image.</p>
								<ol>
									<li>Confidentiality</li>
									<li>Openness</li>
									<li>Competency</li>
									<li>Impartiality</li>
									<li>Fair level of assessment</li>
									<li>Mutually beneficial</li>
									<li>Customer focus</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<div class="about">
					<div class="row">
						<div class="col-md-6">
							<!-- Heading and para -->
							<div class="block-heading-two">
								<h3><span>Mission</span></h3>
							</div>
							<p>QR Services dedicates its experience for building up confidential societies to the end users avail genuine deliverable from dependent suppliers. Achieve 100% purity in certifications is the theme driving QRS for several decades.</p>
						</div>
						<div class="col-md-6">
							<div class="block-heading-two">
								<h3><span>Vision</span></h3>
							</div>
							<p> Registrations for non-profitability and principle oriented assessment of systems to reach the untouched markets. Maintain the confidentiality with clients and high levels of competency is the objective driven from our policies.</p>
						</div>
					</div>
					<br>
		</section>
		<?php include 'includes/footer.php'; ?>
	</div>
	<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
	<!-- javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/animate.js"></script>
	<!-- Vendor Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/animate.js"></script>
	<script src="js/custom.js"></script>
</body>

</html>
