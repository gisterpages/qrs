<!DOCTYPE html>
<html lang="en">

<?php include 'includes/head.php'; ?>

<body>
  <div id="wrapper" class="home-page">
    <!-- start header -->
    <?php include 'includes/topbar.php'; ?>
    <?php include 'includes/header.php'; ?>
    <!-- end header -->

    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="pageTitle">Management Systems</h2>
          </div>
        </div>
      </div>
    </section>
    <section id="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="about-logo">
            <h3>Management Systems</span></h3>
            <p> A management system is developed by organizations against core parameters like Quality, Environmental, health and safety, Food safety, Information security etc. An organization implementing a management system in accordance to ISO standards have to undergo conformity assessment with Conformity Assessment Bodies, generally known as certification bodies. A certified or registered management system instills confidence on the organization in the supply-chain demand. </p>
            <p>A management system describes the set of procedures an organization needs to follow in order to meet its objectives. In a small organization there may not be an official system, just ‘our way of doing things’. Often ‘our way of doing things’ is not written down, instead it is in the head of the staff. However, the larger the organization the more likely it is that there are written instructions about how things are done. This makes sure that nothing is left out and that everyone is clear about who needs to do what, when and how. When an organization systemizes how it does things, this is known as a management system</p>
            <p>The key process to get certified for a management system is to successfully compete and complete conformity assessment bodies auditing process. An audit is a standard way of assessing organizations or personnel against set requirements. Rather than inspection, an audit is a systematic way of approaching a management system and collecting objective evidence to identify conformities and non-conformities. All ISO's management system standards are based on the principle of continual improvement. An organization or company assesses its current situation, fixes objectives and develops policy, implements actions to meet these objectives and then measures the results. With this information the effectiveness of the policy, and the actions taken to achieve it, can be continually reviewed and improved.</p>
            <h4>Principles of Management Systems</h4>
            <ol>
              <li>Customer focus</li>
              <li>Leadership</li>
              <li>Involvement of people</li>
              <li>Process approach</li>
              <li>System approach to management</li>
              <li>Continual improvement</li>
              <li>Factual approach to decision making</li>
              <li>Mutually beneficial to suppliers</li>
            </ol>
          </div>
        </div>
        <div class="col-md-6">
          <br />
          <img src="img/img1.png" class="iso-image">
        </div>
      </div>
    </div>
    </section>
    <!-- start footer -->
    <?php include 'includes/footer.php'; ?>
    <!-- end footer -->
  </div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
  <!-- javascript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.fancybox.pack.js"></script>
  <script src="js/jquery.fancybox-media.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/animate.js"></script>
  <!-- Vendor Scripts -->
  <script src="js/modernizr.custom.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/animate.js"></script>
  <script src="js/custom.js"></script>
</body>

</html>
